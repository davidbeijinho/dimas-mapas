/**
 * This file was @generated using pocketbase-typegen
 */

import type PocketBase from "pocketbase";
import type { RecordService } from "pocketbase";

export enum Collections {
  Authors = "authors",
  Location = "location",
  Places = "places",
  Routes = "routes",
  Users = "users",
}

// Alias types for improved usability
export type IsoDateString = string;
export type RecordIdString = string;
export type HTMLString = string;

// System fields
export type BaseSystemFields<T = never> = {
  id: RecordIdString;
  created: IsoDateString;
  updated: IsoDateString;
  collectionId: string;
  collectionName: Collections;
  expand?: T;
};

export type AuthSystemFields<T = never> = {
  email: string;
  emailVisibility: boolean;
  username: string;
  verified: boolean;
} & BaseSystemFields<T>;

// Record types for each collection

export type AuthorsRecord = {
  born?: IsoDateString;
  description?: HTMLString;
  die?: IsoDateString;
  gallery?: string[];
  name: string;
  picture?: string;
  work_with?: RecordIdString[];
};

export type LocationRecord = {
  description?: HTMLString;
  name?: string;
};

export type PlacesRecord = {
  authors?: RecordIdString[];
  coordinates: string;
  description?: HTMLString;
  gallery?: string[];
  location?: RecordIdString;
  name: string;
  picture?: string;
  year_begin?: number;
  year_end?: number;
};

export type RoutesRecord = {
  description?: HTMLString;
  name?: string;
  places: RecordIdString[];
};

export type UsersRecord = {
  avatar?: string;
  name?: string;
};

// Response types include system fields and match responses from the PocketBase API
export type AuthorsResponse<Texpand = unknown> = Required<AuthorsRecord> &
  BaseSystemFields<Texpand>;
export type LocationResponse<Texpand = unknown> = Required<LocationRecord> &
  BaseSystemFields<Texpand>;
export type PlacesResponse<Texpand = unknown> = Required<PlacesRecord> &
  BaseSystemFields<Texpand>;
export type RoutesResponse<Texpand = unknown> = Required<RoutesRecord> &
  BaseSystemFields<Texpand>;
export type UsersResponse<Texpand = unknown> = Required<UsersRecord> &
  AuthSystemFields<Texpand>;

// Types containing all Records and Responses, useful for creating typing helper functions

export type CollectionRecords = {
  authors: AuthorsRecord;
  location: LocationRecord;
  places: PlacesRecord;
  routes: RoutesRecord;
  users: UsersRecord;
};

export type CollectionResponses = {
  authors: AuthorsResponse;
  location: LocationResponse;
  places: PlacesResponse;
  routes: RoutesResponse;
  users: UsersResponse;
};

// Type for usage with type asserted PocketBase instance
// https://github.com/pocketbase/js-sdk#specify-typescript-definitions

export type TypedPocketBase = PocketBase & {
  collection(idOrName: "authors"): RecordService<AuthorsResponse>;
  collection(idOrName: "location"): RecordService<LocationResponse>;
  collection(idOrName: "places"): RecordService<PlacesResponse>;
  collection(idOrName: "routes"): RecordService<RoutesResponse>;
  collection(idOrName: "users"): RecordService<UsersResponse>;
};
