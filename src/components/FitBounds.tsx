import { LatLngTuple } from "leaflet";
import { PlacesResponse } from "@/../pocketbase-types";
import { ListResult } from "pocketbase";
import { useMap } from "react-leaflet";

export default function FitBounds({
  places,
}: {
  places: ListResult<PlacesResponse>;
}) {
  const line = places.items.map((place) =>
    place.coordinates.split(",").map(Number),
  );
  const map = useMap();
  map.fitBounds(line as LatLngTuple[]);
  return <></>;
}
