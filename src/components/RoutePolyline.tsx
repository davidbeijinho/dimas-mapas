import { Polyline } from "react-leaflet/Polyline";
import type { LatLngTuple } from "leaflet";
import { useMap } from "react-leaflet";

export default function RoutePolyline({ line }: { line: LatLngTuple[] }) {
  const map = useMap();
  map.fitBounds(line);
  return <Polyline pathOptions={{ color: "black" }} positions={line} />;
}
