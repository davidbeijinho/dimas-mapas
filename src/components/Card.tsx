export default function Card({ children }: { children: React.ReactNode }) {
  return (
    <div className="border-border dark:border-darkBorder shadow-light dark:shadow-dark rounded-base border-2  p-5 transition-all hover:translate-x-boxShadowX hover:translate-y-boxShadowY hover:shadow-none dark:hover:shadow-none">
      {children}
    </div>
  );
}
