import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Image from "next/image";

export default function ImageGallery({ list }: { list: string[] }) {
  const settings = {
    // customPaging: function(i) {
    //   return (
    //     <a className="">
    //       <img src={list[i]} />
    //     </a>
    //   );
    // },
    dots: true,
    dotsClass: "slick-dots slick-thumb",
    // infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    // adaptiveHeight:false,
    // variableWidth:false,
    // centerMode: true,
  };
  return (
    <div className="slider-container  mb-20  mx-5">
      <Slider {...settings}>
        {list.map((i) => (
          <div key={i}>
            <Image
              alt="image gallery"
              src={i}
              width={1000}
              height={1000}
              className="max-h-[80vh] w-full h-full object-contain"
            />
          </div>
        ))}
      </Slider>
    </div>
  );
}
