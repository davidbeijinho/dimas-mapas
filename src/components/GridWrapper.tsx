import React from "react";

export default function GridWrapper({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <div className="justify-end xl:flex mr-1 sm:mr-0">
      <div className="dark:text-text grid w-full grid-cols-1 sm:grid-cols-2 gap-10 md:grid-cols-3 xl:pb-16 sm:gap-7">
        {children}
      </div>
    </div>
  );
}
