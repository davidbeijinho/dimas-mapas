import type { RoutesResponse } from "@/../pocketbase-types";
import Link from "next/link";

export default function RoutesList({ routes }: { routes: RoutesResponse[] }) {
  return routes.map((route) => (
    <Link
      href={`/routes/${route.id}`}
      className="block text-blue-600"
      key={route.id}
    >
      {route.name}
    </Link>
  ));
}
