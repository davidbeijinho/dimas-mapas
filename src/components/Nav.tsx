"use client";

import { usePathname } from "next/navigation";
import clsx from "clsx";

import Link from "next/link";

export default function Nav() {
  const path = usePathname();

  const links = [
    {
      path: "/",
      text: "Home",
      isActive: (path: string) => path === "/",
    },
    {
      path: "/authors",
      text: "Authors",
      isActive: (path: string) => path.startsWith("/authors"),
    },
    {
      path: "/places",
      text: "Places",
      isActive: (path: string) => path.startsWith("/places"),
    },
    {
      path: "/map/map",
      text: "Map",
      isActive: (path: string) => path.startsWith("/map"),
    },
  ];

  return (
    <div className="fixed left-0 top-5 z-50 w-full">
      <nav className="text-text border-border dark:border-darkBorder shadow-light dark:shadow-dark mx-auto flex w-max gap-5 rounded-base border-2 bg-main p-2.5 px-5 text-sm font-base sm:text-base sm:gap-4">
        {links.map((link) => {
          return (
            <Link
              key={link.path}
              className={clsx(
                "hover:border-border dark:hover:border-darkBorder rounded-base border-2 px-2 py-1 transition-colors",
                link.isActive(path)
                  ? "border-border dark:border-darkBorder"
                  : "border-transparent",
              )}
              href={link.path}
            >
              {link.text}
            </Link>
          );
        })}
      </nav>
    </div>
  );
}
