import type { PlacesResponse } from "@/../pocketbase-types";
import Link from "next/link";
import ImageCard from "@/components/ImageCard";
import { getImageUrl } from "@/lib/pocketbase";

export default function PlacesList({ list }: { list: PlacesResponse[] }) {
  return list?.map((place) => (
    <Link key={place.id} href={`/places/${place.id}`}>
      <ImageCard
        alt={place.name}
        width={600}
        height={600}
        src={getImageUrl({
          collection: "places",
          filename: place.picture,
          record: place.id,
        })}
      >
        {place.name}
      </ImageCard>
    </Link>
  ));
}
