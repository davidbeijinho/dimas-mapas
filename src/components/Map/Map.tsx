import dynamic from "next/dynamic";

const DynamicMap = dynamic(() => import("./DynamicMap"), {
  ssr: false,
});

// Set default sizing to control aspect ratio which will scale responsively
// but also help avoid layout shift

const DEFAULT_WIDTH = 600;
const DEFAULT_HEIGHT = 600;

export default function Map(props: any) {
  const { width = DEFAULT_WIDTH, height = DEFAULT_HEIGHT } = props;
  // const aspectRatio = width / height;
  const aspectRatio = "auto";
  return (
    <div style={{ aspectRatio }}>
      <DynamicMap {...props} />
    </div>
  );
}
