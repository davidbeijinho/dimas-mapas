import { useEffect } from "react";
import Leaflet from "leaflet";
import { MapContainer } from "react-leaflet/MapContainer";
import "leaflet/dist/leaflet.css";

const Map = ({
  children,
  className,
  width,
  height,
  ...rest
}: {
  children: React.ReactNode;
  className: string;
  width: string;
  height: string;
}) => {
  let mapClassName = "";

  if (className) {
    mapClassName = ` ${mapClassName} ${className}`;
  }

  useEffect(() => {
    (async function init() {
      // delete (Leaflet as any).Icon.Default.prototype._getIconUrl;
      Leaflet.Icon.Default.mergeOptions({
        iconRetinaUrl: "/leaflet/images/marker-icon-2x.png",
        iconUrl: "/leaflet/images/marker-icon.png",
        shadowUrl: "/leaflet/images/marker-shadow.png",
      });
    })();
  }, []);

  return (
    <MapContainer
      className={mapClassName}
      {...rest}
      style={{
        minWidth: "100%",
        minHeight: "80vh",
        width: "100%",
        height: "100%",
      }}
    >
      {children}
    </MapContainer>
  );
};

export default Map;
