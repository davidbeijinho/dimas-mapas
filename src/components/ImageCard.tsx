import Card from "@/components/Card";
import Image from "next/image";

export default function ImageCard({
  children,
  alt,
  src,
  width,
  height,
}: {
  children: React.ReactNode;
  alt: string;
  src: string;
  width: number;
  height: number;
}) {
  return (
    <Card>
      <div className="w-100 max-h-60 overflow-hidden">
        <Image
          className="w-full"
          alt={alt}
          width={width}
          height={height}
          src={src}
        />
      </div>
      <p className="mt-3 text-lg font-heading sm:text-xl">{children}</p>
    </Card>
  );
}
