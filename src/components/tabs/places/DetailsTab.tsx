import { getImageUrl } from "@/lib/pocketbase";
import Image from "next/image";
import { PlacesResponse } from "@/../pocketbase-types";
import ImageGallery from "@/components/ImageGallery";

export default function DetailsTab({ place }: { place: PlacesResponse }) {
  const imagesList = place?.gallery
    ? [
        getImageUrl({
          collection: "places",
          filename: place.picture,
          record: place.id,
        }),
        ...(place.gallery?.length
          ? place.gallery.map((image) =>
              getImageUrl({
                collection: "places",
                filename: image,
                record: place.id,
              }),
            )
          : []),
      ]
    : [];
  return (
    <>
      {imagesList?.length > 1 ? (
        <ImageGallery list={imagesList} />
      ) : (
        <Image
          className="max-h-[80vh] w-full h-full object-contain"
          width={1000}
          height={1000}
          alt={place.name}
          src={imagesList[0]}
        />
      )}
      <hr />
      <div dangerouslySetInnerHTML={{ __html: place.description }} />
    </>
  );
}
