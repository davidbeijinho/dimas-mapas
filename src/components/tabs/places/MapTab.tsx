import { PlacesResponse } from "@/../pocketbase-types";
import Loading from "@/components/Loading";
import PlacesMap from "@/components/PlacesMap";

export default function MapTab({ place }: { place: PlacesResponse }) {
  return place?.coordinates ? (
    <PlacesMap
      places={{
        items: [place],
        page: 1,
        perPage: 1,
        totalItems: 1,
        totalPages: 1,
      }}
      center={["0", "0"]}
    />
  ) : (
    <Loading />
  );
}
