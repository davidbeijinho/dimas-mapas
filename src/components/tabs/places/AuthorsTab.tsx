import Link from "next/link";
import { AuthorsResponse, PlacesResponse } from "@/../pocketbase-types";
import GridWrapper from "@/components/GridWrapper";
import Card from "@/components/Card";
type Texpand = {
  authors: AuthorsResponse[];
};
export default function AuthorsTab({
  place,
}: {
  place: PlacesResponse<Texpand>;
}) {
  return place?.expand?.authors ? (
    <GridWrapper>
      {place?.expand?.authors.map((author) => (
        <Link
          key={author.id}
          className="block mb-5"
          href={`/authors/${author.id}`}
        >
          <Card>{author.name}</Card>
        </Link>
      ))}
    </GridWrapper>
  ) : (
    <p>aqui nao ha Autores porque o Fabinho nao adicionou</p>
  );
}
