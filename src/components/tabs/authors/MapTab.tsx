import { AuthorsResponse, PlacesResponse } from "@/../pocketbase-types";
import PlacesMap from "@/components/PlacesMap";
type Texpand = {
  places_via_authors: PlacesResponse[];
};
export default function MapTab({
  author,
}: {
  author: AuthorsResponse<Texpand>;
}) {
  return author?.expand?.places_via_authors ? (
    <PlacesMap
      places={{
        items: author.expand.places_via_authors,
        page: 1,
        perPage: 1,
        totalItems: 1,
        totalPages: 1,
      }}
      center={["0", "0"]}
    />
  ) : (
    "No Places Found"
  );
}
