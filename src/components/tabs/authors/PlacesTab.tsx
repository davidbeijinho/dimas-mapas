import { AuthorsResponse, PlacesResponse } from "@/../pocketbase-types";
import GridWrapper from "@/components/GridWrapper";
import PlacesList from "@/components/PlacesList";
type Texpand = {
  places_via_authors: PlacesResponse[];
};
export default function PlacesTab({
  author,
}: {
  author: AuthorsResponse<Texpand>;
}) {
  return author?.expand?.places_via_authors ? (
    <GridWrapper>
      <PlacesList list={author.expand.places_via_authors} />
    </GridWrapper>
  ) : (
    "No Places Found"
  );
}
