import { getImageUrl } from "@/lib/pocketbase";
import Image from "next/image";
import { AuthorsResponse } from "@/../pocketbase-types";

export default function DetailsTab({ author }: { author: AuthorsResponse }) {
  return (
    <>
      {author.picture && (
        <Image
          className="w-32"
          width={100}
          height={100}
          alt={author.name}
          src={getImageUrl({
            collection: "authors",
            filename: author.picture,
            record: author.id,
          })}
        />
      )}
      <hr />
      <div dangerouslySetInnerHTML={{ __html: author.description }} />
    </>
  );
}
