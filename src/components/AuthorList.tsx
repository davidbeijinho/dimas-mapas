import type { AuthorsResponse } from "@/../pocketbase-types";
import Link from "next/link";
import Card from "@/components/Card";

export default function AuthorsList({ list }: { list: AuthorsResponse[] }) {
  return list?.map((author) => (
    <Link key={author.id} href={`/authors/${author.id}`}>
      <Card>
        <p className="text-lg font-heading sm:text-xl">{author.name}</p>
      </Card>
    </Link>
  ));
}
