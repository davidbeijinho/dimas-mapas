import {
  TypedPocketBase,
  PlacesResponse,
  AuthorsResponse,
  Collections,
} from "@/../pocketbase-types";
import PocketBase from "pocketbase";

const URL = "https://pocketbase.hive.thebeijinho.com";
const pb = new PocketBase(URL) as TypedPocketBase;

export const getPlaces = async () => {
  const result = await pb
    .collection(Collections.Places)
    .getList<PlacesResponse>(1, 50, {
      sort: "name",
    });
  return result;
};

type TexpandPlace = {
  authors: AuthorsResponse[];
};
export const getPlace = async ({ id }: { id: string }) => {
  const result = await pb
    .collection(Collections.Places)
    .getOne<PlacesResponse<TexpandPlace>>(id, {
      expand: "authors",
    });

  return result;
};

export const getAuthors = async () => {
  const result = await pb
    .collection(Collections.Authors)
    .getList<AuthorsResponse>(1, 50, { sort: "name" });

  return result;
};
type TexpandAuthor = {
  places_via_authors: PlacesResponse[];
};
export const getAuthor = async ({ id }: { id: string }) => {
  const result = await pb
    .collection(Collections.Authors)
    .getOne<
      AuthorsResponse<TexpandAuthor>
    >(id, { expand: "places_via_authors" });
  return result;
};

export const getImageUrl = ({
  collection,
  record,
  filename,
}: {
  collection: string;
  record: string;
  filename: string;
}) => {
  //http://127.0.0.1:8090/api/files/COLLECTION_ID_OR_NAME/RECORD_ID/FILENAME

  // const url = pb.files.getUrl(record, firstFilename, {'thumb': '100x250'});
  // return url
  // ?token=
  return `${URL}/api/files/${collection}/${record}/${filename}`;
};
