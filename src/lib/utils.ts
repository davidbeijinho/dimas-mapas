import slugify from "slugify";
import { clsx, type ClassValue } from "clsx";
import { twMerge } from "tailwind-merge";

export const slugToId = (slug: string) => {
  return slug.split("-")[0];
};

export const toBlogSlug = ({ id, name }: { id: string; name: string }) => {
  return `/blog/${id}-${slugify(name)}`;
};

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs));
}
