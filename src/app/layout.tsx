import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";
import Nav from "@/components/Nav";
const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "O mapas do dimas",
  description: "o mais bonito da aldeia",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <Nav />
        <div className="text-text dark:text-darkText mx-auto w-[1850px] max-w-full px-1 sm:px-5 pb-10 pt-28">
          {children}
        </div>
      </body>
    </html>
  );
}
