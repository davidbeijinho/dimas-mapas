"use client";
import Tabs from "@/components/Tabs";
import { getAuthor } from "@/lib/pocketbase";
import React, { useEffect, useState } from "react";
import { AuthorsResponse, PlacesResponse } from "@/../pocketbase-types";
import DetailsTab from "@/components/tabs/authors/DetailsTab";
import MapTab from "@/components/tabs/authors/MapTab";
import PlacesTab from "@/components/tabs/authors/PlacesTab";
import Loading from "@/components/Loading";
type Texpand = {
  places_via_authors: PlacesResponse[];
};
export default function Page({ params }: { params: { slug: string } }) {
  const [author, setAuthor] = useState<AuthorsResponse<Texpand> | null>(null);

  useEffect(() => {
    async function fetchData() {
      try {
        const response = await getAuthor({ id: params.slug });
        setAuthor(response);
      } catch (error) {
        console.log("error", error);
      }
    }

    fetchData();
  }, [params.slug]);
  const [activeTab, setActiveTab] = useState("Details");
  const tabsArray = ["Details", "Places", "Map"];
  return author?.name ? (
    <>
      <h1 className="text-center text-4xl font-heading mb-4">{author.name}</h1>
      <div className=" rounded-base w-full">
        <Tabs
          activeTab={activeTab}
          setActiveTab={setActiveTab}
          tabsArray={tabsArray}
        />
        <div className="max-w-full rounded-b-base border-2 border-border bg-white p-5 font-base">
          {activeTab === "Details" && <DetailsTab author={author} />}
          {activeTab === "Map" && <MapTab author={author} />}
          {activeTab === "Places" && <PlacesTab author={author} />}
        </div>
      </div>
    </>
  ) : (
    <Loading />
  );
}
