"use client";
import { getAuthors } from "@/lib/pocketbase";
import React, { useEffect, useState } from "react";
import GridWrapper from "@/components/GridWrapper";
import AuthorsList from "@/components/AuthorList";
import Loading from "@/components/Loading";
import { AuthorsResponse } from "@/../pocketbase-types";
import { ListResult } from "pocketbase";
export default function Page() {
  const [authors, setAuthors] = useState<ListResult<AuthorsResponse> | null>(
    null,
  );

  useEffect(() => {
    async function fetchData() {
      try {
        const response = await getAuthors();
        setAuthors(response);
      } catch (error) {
        console.log("error", error);
      }
    }

    fetchData();
  }, []);

  return authors?.items ? (
    <GridWrapper>
      <AuthorsList list={authors.items} />
    </GridWrapper>
  ) : (
    <Loading />
  );
}
