"use client";
import PlacesMap from "@/components/PlacesMap";
import { useEffect, useState } from "react";
import { getPlaces } from "@/lib/pocketbase";
import { ListResult } from "pocketbase";
import { PlacesResponse } from "@/../pocketbase-types";
import Loading from "@/components/Loading";
export default function Page() {
  const [places, setPlaces] = useState<ListResult<PlacesResponse> | null>(null);
  useEffect(() => {
    async function fetchData() {
      try {
        const response = await getPlaces();
        setPlaces(response);
      } catch (error) {
        console.log("error", error);
      }
    }
    fetchData();
  }, []);
  return places?.items ? (
    <PlacesMap places={places} center={["0", "0"]} />
  ) : (
    <Loading />
  );
}
