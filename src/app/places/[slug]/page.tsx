"use client";
import { getPlace } from "@/lib/pocketbase";
import React, { useEffect, useState } from "react";
import { AuthorsResponse, PlacesResponse } from "@/../pocketbase-types";
import Tabs from "@/components/Tabs";
import Loading from "@/components/Loading";
import MapTab from "@/components/tabs/places/MapTab";
import AuthorsTab from "@/components/tabs/places/AuthorsTab";
import DetailsTab from "@/components/tabs/places/DetailsTab";
type TexpandPlace = {
  authors: AuthorsResponse[];
};
export default function Page({ params }: { params: { slug: string } }) {
  const [place, setPlace] = useState<PlacesResponse<TexpandPlace> | null>(null);
  useEffect(() => {
    async function fetchData() {
      try {
        const response = await getPlace({ id: params.slug });
        if (response) {
          setPlace(response);
        }
      } catch (error) {
        console.log("error", error);
      }
    }
    fetchData();
  }, [params.slug]);
  const [activeTab, setActiveTab] = useState("Details");
  const tabsArray = ["Details", "Authors", "Map"];

  return place ? (
    <>
      <h1 className="text-center text-4xl font-heading mb-4">{place.name}</h1>
      <div className=" rounded-base w-full">
        <Tabs
          activeTab={activeTab}
          setActiveTab={setActiveTab}
          tabsArray={tabsArray}
        />
        <div className="max-w-full rounded-b-base border-2 border-border bg-white p-5 font-base">
          {activeTab === "Details" && <DetailsTab place={place} />}
          {activeTab === "Map" && <MapTab place={place} />}
          {activeTab === "Authors" && <AuthorsTab place={place} />}
        </div>
      </div>
    </>
  ) : (
    <Loading />
  );
}
