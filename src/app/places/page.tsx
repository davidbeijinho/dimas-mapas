"use client";
import { getPlaces } from "@/lib/pocketbase";
import React, { useEffect, useState } from "react";
import GridWrapper from "@/components/GridWrapper";
import PlacesList from "@/components/PlacesList";
import Loading from "@/components/Loading";
import { PlacesResponse } from "@/../pocketbase-types";
import { ListResult } from "pocketbase";
export default function Page() {
  const [places, setPlaces] = useState<ListResult<PlacesResponse> | null>(null);
  useEffect(() => {
    async function fetchData() {
      try {
        const response = await getPlaces();
        // console.log({map})
        setPlaces(response);
      } catch (error) {
        console.log("error", error);
      }
    }
    fetchData();
  }, []);

  return places?.items ? (
    <GridWrapper>
      <PlacesList list={places.items} />
    </GridWrapper>
  ) : (
    <Loading />
  );
}
